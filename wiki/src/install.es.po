# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2022-02-25 15:40-0600\n"
"PO-Revision-Date: 2020-08-16 21:29+0000\n"
"Last-Translator: Joaquín Serna <bubuanabelas@cryptolab.net>\n"
"Language-Team: Spanish <http://translate.tails.boum.org/projects/tails/"
"install/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.5.1\n"

#. type: Content of: <div>
#, fuzzy
#| msgid "[[!meta title=\"Download and install Tails\"]]"
msgid "[[!meta title=\"Get Tails\"]]"
msgstr "[[!meta title=\"Descargar e instalar Tails\"]]"

#. type: Content of: outside any tag (error?)
#, fuzzy
#| msgid ""
#| "[[!meta stylesheet=\"bootstrap.min\" rel=\"stylesheet\" title=\"\"]] [[!"
#| "meta stylesheet=\"inc/stylesheets/assistant\" rel=\"stylesheet\" title="
#| "\"\"]] [[!meta stylesheet=\"hide-breadcrumbs\" rel=\"stylesheet\" title="
#| "\"\"]]"
msgid ""
"[[!meta stylesheet=\"hide-breadcrumbs\" rel=\"stylesheet\" title=\"\"]] [[!"
"meta stylesheet=\"install\" rel=\"stylesheet\" title=\"\"]]"
msgstr ""
"[[!meta stylesheet=\"bootstrap.min\" rel=\"stylesheet\" title=\"\"]] [[!meta "
"stylesheet=\"inc/stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]] [[!"
"meta stylesheet=\"hide-breadcrumbs\" rel=\"stylesheet\" title=\"\"]]"

#. type: Content of: <div><div>
msgid "[[ [[!img install/inc/icons/windows.png link=\"no\" alt=\"\"]]"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Windows"
msgstr "Windows"

#. type: Content of: <div><div>
#, fuzzy
#| msgid "|install/win]]"
msgid "|install/windows]]"
msgstr "|install/win]]"

#. type: Content of: <div><div>
msgid "[[ [[!img install/inc/icons/apple.png link=\"no\" alt=\"\"]]"
msgstr ""

#. type: Content of: <div><div><p>
msgid "macOS"
msgstr "macOS"

#. type: Content of: <div><div>
msgid "|install/mac]]"
msgstr "|install/mac]]"

#. type: Content of: <div><div>
msgid "[[ [[!img install/inc/icons/linux.png link=\"no\" alt=\"\"]]"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Linux"
msgstr "Linux"

#. type: Content of: <div><div>
msgid "|install/linux]]"
msgstr "|install/linux]]"

#. type: Content of: <div><div>
msgid "[[ [[!img install/inc/icons/expert.png link=\"no\" alt=\"\"]]"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Terminal"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Debian or Ubuntu using the command line and GnuPG"
msgstr ""

#. type: Content of: <div><div>
#, fuzzy
#| msgid "|install/linux]]"
msgid "|install/expert]]"
msgstr "|install/linux]]"

#. type: Content of: <div><p>
msgid ""
"If you know someone you trust who uses Tails already, you can install your "
"Tails by cloning their Tails:"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "[[Install by cloning from another Tails on PC|install/clone/pc]]"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "[[Install by cloning from another Tails on Mac|install/clone/mac]]"
msgstr ""

#. type: Content of: <h3>
msgid "Other options:"
msgstr ""

#. type: Content of: <ul><li>
#, fuzzy
#| msgid "[[For USB sticks (USB image)|install/download]]"
msgid "[[Download only (for USB sticks)|install/download]]"
msgstr "[[Para memorias USB (imagen USB)|install/download]]"

#. type: Content of: <ul><li>
msgid "[[Burn a Tails DVD|install/dvd]]"
msgstr ""

#. type: Content of: <ul><li>
#, fuzzy
#| msgid "[[For virtual machines (ISO image)|install/vm-download]]"
msgid "[[Run Tails in a virtual machine|install/vm]]"
msgstr "[[Para máquinas virtuales (imagen ISO)|install/vm-download]]"

#~ msgid "Thank you for your interest in Tails."
#~ msgstr "Gracias por tu interés en Tails."

#~ msgid ""
#~ "Installing Tails can be quite long but we hope you will still have a good "
#~ "time :)"
#~ msgstr ""
#~ "Instalar Tails es un proceso largo pero esperamos que te lo pases bien :)"

#~ msgid ""
#~ "We will first ask you a few questions to choose your installation "
#~ "scenario and then guide you step by step."
#~ msgstr ""
#~ "Primero te haremos algunas preguntas para elegir un escenario de "
#~ "instalación y guiarte paso a paso."

#~ msgid "Which operating system are you installing Tails from?"
#~ msgstr "¿Desde qué sistema operativo estás instalando Tails?"

#~ msgid "[["
#~ msgstr "[["

#~ msgid "Download only:"
#~ msgstr "Sólo descargar:"

#~ msgid "[[For DVDs (ISO image)|install/dvd-download]]"
#~ msgstr "[[Para DVDs (imagen ISO)|install/dvd-download]]"

#~ msgid "Debian, Ubuntu, or Mint"
#~ msgstr "Debian, Ubuntu, o Mint"

#~ msgid "|install/debian]]"
#~ msgstr "|install/debian]]"

#~ msgid "<small>(Red Hat, Fedora, etc.)</small>"
#~ msgstr "<small>(Red Hat, Fedora, etc.)</small>"

#~ msgid "Let's start the journey!"
#~ msgstr "¡Comencemos el viaje!"

#~ msgid "Welcome to the"
#~ msgstr "Bienvenido al"

#~ msgid "<strong>Tails Installation Assistant</strong>"
#~ msgstr "<strong>Asistente de Instalación de Tails</strong>"
