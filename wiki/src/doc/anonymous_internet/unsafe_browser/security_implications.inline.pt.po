# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2021-05-28 19:01-0500\n"
"PO-Revision-Date: 2022-03-07 23:24+0000\n"
"Last-Translator: drebs <drebs@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta robots=\"noindex\"]]\n"
msgstr "[[!meta robots=\"noindex\"]]\n"

#. type: Plain text
msgid "- **The *Unsafe Browser* is not anonymous.**"
msgstr "- **O *Navegador Inseguro* não é anônimo.**"

#. type: Plain text
#, no-wrap
msgid ""
"  The *Unsafe Browser* does not use Tor. The websites that you visit can\n"
"  see your real IP address.\n"
msgstr ""
"  O *Navegador Inseguro* não usa a rede Tor. Ao usá-lo, os sites que você "
"visita podem\n"
"  ver o seu endereço de IP verdadeiro.\n"

#. type: Plain text
#, no-wrap
msgid "  This is why we recommend that you:\n"
msgstr "  É por isso que te recomendamos:\n"

#. type: Bullet: '  - '
msgid ""
"Only use the *Unsafe Browser* to sign in to a network using a captive portal "
"or [[browse trusted web pages on the local network|advanced_topics/"
"lan#browser]]."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"Close the *Unsafe Browser* after signing in to the network, to avoid using "
"it by mistake."
msgstr ""

#. type: Plain text
msgid "- **The *Unsafe Browser* can be used to deanonymize you.**"
msgstr ""
"-**O *Navegador Inseguro* pode ser usado para acabar com seu anonimato.**"

#. type: Plain text
#, no-wrap
msgid ""
"  An attacker could exploit a security vulnerability in another\n"
"  application in Tails to start an invisible *Unsafe Browser* and reveal\n"
"  your IP address, even if you are not using the *Unsafe Browser*.\n"
msgstr ""
"  Um atacante poderia explorar uma falha de segurança em outra\n"
"  aplicação no Tails para iniciar um *Navegador Inseguro* invisível e "
"revelar\n"
"  seu endereço de IP verdadeiro, mesmo que você não esteja usando o *"
"Navegador Inseguro*.\n"

#. type: Plain text
#, no-wrap
msgid ""
"  For example, an attacker could exploit a security vulnerability in\n"
"  *Thunderbird* by sending you a [phishing\n"
"  email](https://ssd.eff.org/en/module/how-avoid-phishing-attacks) that\n"
"  could start an invisible *Unsafe Browser* and reveal them your IP\n"
"  address.\n"
msgstr ""
"  Por exemplo, um atacante poderia explorar uma falha de segurança no\n"
"  *Thunderbird* ao enviar para você um [email de\n"
"  phishing](https://ssd.eff.org/en/module/how-avoid-phishing-attacks) que\n"
"  poderia iniciar um *Navegador Inseguro* invisivel e revelar o seu\n"
"  endereço de IP verdadeiro.\n"

#. type: Plain text
#, no-wrap
msgid ""
"  Such an attack is very unlikely but could be performed by a strong\n"
"  attacker, such as a government or a hacking firm.\n"
msgstr ""
"  Um ataque desses é muito improvável, mas poderia ser conduzido por um\n"
"  atacante forte, tal como um governo ou uma empresa de hackers.\n"

#. type: Bullet: '  - '
msgid ""
"Only enable the *Unsafe Browser* if you need to sign to a network using a "
"captive portal."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"Always upgrade to the latest version of Tails to fix known vulnerabilities "
"as soon as possible."
msgstr ""
"Sempre atualize para a última versão do Tails para consertar "
"vulnerabilidades conhecidas o quanto antes."

#. type: Plain text
#, no-wrap
msgid ""
"  We have plans to fix the root cause of this problem but it requires\n"
"  [[!tails_ticket 12213 desc=\"important engineering work\"]].\n"
msgstr ""
