# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2021-07-01 12:11+0200\n"
"PO-Revision-Date: 2015-10-25 17:35+0000\n"
"Last-Translator: sprint5 <translation5@451f.org>\n"
"Language-Team: Persian <http://weblate.451f.org:8889/projects/tails/"
"aboutindex/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.4-dev\n"

#. type: Bullet: '  - '
msgid "[[!traillink System_requirements|about/requirements]]"
msgstr "[[!traillink نیازمندی‌های_سیستم|about/requirements]]"

#. type: Plain text
#, no-wrap
msgid ""
"  - [[!traillink Warnings:_Tails_is_safe_but_not_magic!|about/warnings]]\n"
"    - [[!traillink Protecting_your_identity_when_using_Tails|about/warnings/identity]]\n"
"    - [[!traillink Limitations_of_the_Tor_network|about/warnings/tor]]\n"
"    - [[!traillink Reducing_risks_when_using_untrusted_computers|about/warnings/computer]]\n"
"  - [[!traillink Features_and_included_software|about/features]]\n"
"  - [[!traillink Social_Contract|doc/about/social_contract]]\n"
"  - [[!traillink Trusting_Tails|about/trust]]\n"
"  - [[!traillink License_and_source_code_distribution|about/license]]\n"
"  - [[!traillink Acknowledgments_and_similar_projects|about/acknowledgments_and_similar_projects]]\n"
"  - [[!traillink Finances|about/finances]]\n"
"  - [[!traillink Contact|about/contact]]\n"
"    - [[!traillink OpenPGP_keys|about/openpgp_keys]]\n"
msgstr ""

#, fuzzy
#~| msgid "[[!traillink Warnings!|about/warning]]"
#~ msgid "[[!traillink Warnings_and_limitations|about/warning]]"
#~ msgstr "[[!traillink هشدارها!|about/warning]]"

#~ msgid "[[!traillink Features_and_included_software|about/features]]"
#~ msgstr "[[!traillink ویژگی‌ها_و_نرم‌افزارهای_گنجانده‌شده|about/features]]"

#~ msgid "[[!traillink Why_does_Tails_use_Tor?|about/tor]]"
#~ msgstr "[[!traillink چرا_تیلز_از_تور_استفاده_می‌کند؟|about/tor]]"

#~ msgid ""
#~ "[[!traillink Can_I_hide_the_fact_that_I_am_using_Tails?|about/"
#~ "fingerprint]]"
#~ msgstr ""
#~ "[[!traillink آیا_می‌توانم_استفاده‌ام_از_تیلز_را_مخفی_کنم؟|about/"
#~ "fingerprint]]"

#, fuzzy
#~| msgid "[[!traillink Finances|about/finances]]"
#~ msgid "[[!traillink Social_Contract|doc/about/social_contract]]"
#~ msgstr "[[!traillink مسائل_مالی|about/finances]]"

#~ msgid "[[!traillink Trusting_Tails|about/trust]]"
#~ msgstr "[[!traillink اعتماد_به_تیلز|about/trust]]"

#, fuzzy
#~| msgid "[[!traillink License|about/license]]"
#~ msgid "[[!traillink License_and_source_code_distribution|about/license]]"
#~ msgstr "[[!traillink گواهی|about/license]]"

#~ msgid ""
#~ "[[!traillink Acknowledgments_and_similar_projects|about/"
#~ "acknowledgments_and_similar_projects]]"
#~ msgstr ""
#~ "[[!traillink قدردانی‌ها_و_پروژه‌های_مشابه|about/"
#~ "acknowledgments_and_similar_projects]]"

#~ msgid "[[!traillink Finances|about/finances]]"
#~ msgstr "[[!traillink مسائل_مالی|about/finances]]"

#, fuzzy
#~| msgid "[[!traillink Finances|about/finances]]"
#~ msgid ""
#~ "  - [[!traillink Contact|about/contact]]\n"
#~ "    - [[!traillink OpenPGP_keys|about/openpgp_keys]]\n"
#~ msgstr "[[!traillink مسائل_مالی|about/finances]]"
