# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2021-11-19 17:07+0000\n"
"PO-Revision-Date: 2021-11-19 18:52+0000\n"
"Last-Translator: nihei <nihei@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.11.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Sat, 12 Apr 2014 20:23:45 +0000\"]]\n"
msgstr "[[!meta date=\"Sat, 12 Apr 2014 20:23:45 +0000\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"And the winner is...\"]]\n"
msgstr "[[!meta title=\"Et le gagnant est...\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
msgid ""
"Our [[logo contest|logo_contest]] ended up a few days ago.  Since then, 11 "
"regular Tails contributors voted on the [[!tails_blueprint logo desc=\"36 "
"proposals\"]]."
msgstr ""
"Notre [[concours de création de logo|logo_contest]] s'est terminé il y a "
"quelques jours. Depuis, 11 personnes contribuant régulièrement à Tails ont "
"votés pour les [[!tails_blueprint logo desc=\"36 propositions\"]]."

#. type: Title =
#, no-wrap
msgid "Winner"
msgstr "Gagnant"

#. type: Plain text
msgid ""
"The winning proposal is the one by [[!tails_blueprint logo#tchou desc=\"Tchou"
"\"]]."
msgstr ""
"La proposition retenue est celle proposée par [[!tails_blueprint logo#tchou "
"desc=\"Tchou\"]]."

#. type: Plain text
msgid ""
"We commented on the initial version and we already came up with an improved "
"version: [[!tails_blueprint logo/tchou-improved.png]]"
msgstr ""
"Nous avons commenté la première version et pouvons vous présenter la version "
"améliorée : [[!tails_blueprint logo/tchou-improved.png]]"

#. type: Plain text
msgid "Congratulations!"
msgstr "Félicitations !"

#. type: Plain text
msgid ""
"In the coming days we will keep on fine-tuning it and integrating it in time "
"for Tails 1.0. So don't hesitate to comment on it."
msgstr ""
"Pendant les prochains jours, nous allons terminer les dernières finitions "
"sur le logo et l'intégrer à Tails 1.0. N'hésitez pas à le commenter."

#. type: Title =
#, no-wrap
msgid "Top 7"
msgstr "Top 7"

#. type: Plain text
msgid "Six other great proposals made it to the top 7:"
msgstr "Six autres excellentes propositions rassemblée dans un top 7 :"

#. type: Bullet: '  - '
msgid ""
"2nd: tie between [[!tails_blueprint logo#andrew desc=\"Andrew\"]] and [[!"
"tails_blueprint logo#joe desc=\"Joe\"]]"
msgstr ""
"Second : Ex aequo entre [[!tails_blueprint logo#andrew desc=\"Andrew\"]] et "
"[[!tails_blueprint logo#joe desc=\"Joe\"]]"

#. type: Bullet: '  - '
msgid ""
"4th: tie between [[!tails_blueprint logo#jared desc=\"Jared\"]] and [[!"
"tails_blueprint logo#renato desc=\"Renato\"]]"
msgstr ""
"Quatrième : Ex aequo entre [[!tails_blueprint logo#jared desc=\"Jared\"]] et "
"[[!tails_blueprint logo#renato desc=\"Renato\"]]"

#. type: Bullet: '  - '
msgid ""
"6th: tie between [[!tails_blueprint logo#mewchan desc=\"MewChan, hiding cat"
"\"]] and [[!tails_blueprint logo#christopher desc=\"Christopher\"]]"
msgstr ""
"Sixième : Ex aequo entre [[!tails_blueprint logo#mewchan desc=\"MewChan, "
"hiding cat\"]] et [[!tails_blueprint logo#christopher desc=\"Christopher\"]]"

#. type: Plain text
msgid ""
"This [[!tails_blueprint logo/top7.pdf desc=\"PDF\"]] shows a graph of how "
"many voters preferred a given proposal to another one."
msgstr ""
"Ce [[!tails_blueprint logo/top7.pdf desc=\"PDF\"]] montre un graphique du "
"nombre de votants pour chaque proposition."

#. type: Plain text
msgid ""
"We reiterate our thanks to the 31 designers who worked for this contest to "
"be such a success."
msgstr ""
"Nous réitérons nos remerciements aux 31 graphistes qui ont fait le succès de "
"ce concours."

#~ msgid "[[!img blueprint/logo/tchou-improved.png link=\"no\"]]\n"
#~ msgstr "[[!img blueprint/logo/tchou-improved.png link=\"no\"]]\n"
