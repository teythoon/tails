# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2021-05-10 15:03+0000\n"
"PO-Revision-Date: 2021-05-22 17:51+0000\n"
"Last-Translator: dedmoroz <cj75300@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<="
"4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 3.11.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta robots=\"noindex\"]]\n"
msgstr "[[!meta robots=\"noindex\"]]\n"

#. type: Plain text
msgid ""
"Importing OpenPGP public keys using the *Passwords and Keys* utility is "
"broken since Tails 4.0 (October 2019). ([[!tails_ticket 17183]])"
msgstr ""

#. type: Plain text
msgid "Do so from the *Files* browser instead:"
msgstr ""

#. type: Bullet: '1. '
msgid "Choose **Applications**&nbsp;▸ **Files** to open the *Files* browser."
msgstr ""

#. type: Bullet: '1. '
msgid ""
"Double-click on the OpenPGP public key that you downloaded. The key must be "
"in the *armored* format, usually with a `.asc` extension."
msgstr ""

#. type: Bullet: '1. '
msgid "Choose **Open With Import Key**."
msgstr ""

#. type: Bullet: '1. '
msgid ""
"The imported OpenPGP public key does not appear in the *Passwords and Keys* "
"utility. But, the key should appear in the list of keys available for "
"encryption when [[encrypting text with a public key|doc/"
"encryption_and_privacy/gpgapplet/public-key_cryptography]] using *OpenPGP "
"Applet*."
msgstr ""
